
export class ManufacturerListItem {

  companyName: string;
  openYear: number;
  closeYear: number;
  numCapacitorTypes: number;
  numCapacitorUnits: number;

}

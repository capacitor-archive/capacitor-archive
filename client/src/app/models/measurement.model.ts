import {Unit} from './measurement-unit.model';

export class Measurement {

  measurementStr: string;
  measurementPm: number;
  unit: Unit;

  /**
   * Constructs a new Measurement only if measurement and unit are non-null, and unit is a valid Unit.
   * Else returns null.
   * @param measurement a measurement that may be null/undefined
   * @param unit a unit that may be null/undefined
   */
  public static fromNullValues(measurement: string, unit: string): Measurement {

    if (measurement && unit && (Object.values(Unit).includes(unit))) {
      return new Measurement(measurement, unit);
    } else {
      return null;
    }
  }

  /**
   * Takes either Measurement(measurement: Measurement) or Measurement(measurementStr: string, unit: Unit)
   * @param measurement either a Measurement object or a measurementStr
   * @param unit either a number corresponding to the Unit enum or a string that matches a string in the Unit enum.
   */
  constructor(measurement?: (string | Measurement), unit?: (number | string | Unit)) {

    if (measurement) {

      if (typeof measurement === 'string') {
        this.measurementStr = measurement;
      } else if (typeof measurement === 'object') {
        this.measurementStr = measurement.measurementStr;
        this.measurementPm = measurement.measurementPm;
        unit = measurement.unit;
      }

      // unit is a number
      if (!isNaN(parseFloat(unit as string)) && isFinite(unit as number)) {
        this.unit = Number(unit) as Unit;

        // unit is a Unit or string
      } else if (typeof unit === 'string') {
        this.unit = Unit[unit] as Unit;
      }

    }
  }


  /** Limits total length to 15 characters */
  public toString(): string {
    return this.measurementStr.trim().substring(0, 12) + ' ' + Unit[this.unit];
  }

  public get unitStr(): string {
    return Unit[this.unit];
  }

  /** Same as unitStr but returns a Unit.mm if this.unit is null/undefined */
  public get unitStrDefault(): string {
    return this.unitStr ? this.unitStr : Unit[Unit.mm];
  }

}

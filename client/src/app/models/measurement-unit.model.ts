
export enum Unit {mm, cm, in}

export const ALL_UNITS = [...Array(Object.keys(Unit).length / 2).keys()].map(i => Unit[i]);

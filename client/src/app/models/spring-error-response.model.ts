
export class SpringErrorResponse {
  error: any;
  message: string;
  path: string;
  status: number;
  timestamp: Date;
}

import {AfterViewInit, Component, ElementRef, Input, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {Subscription} from 'rxjs';
import {caseInsensitiveCompare} from '../../utilities/text-utils';
import {RestService} from '../../services/rest/rest.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CapacitorType} from '../../models/capacitor-type.model';
import {SpringErrorResponse} from '../../models/spring-error-response.model';
import {Location} from '@angular/common';
import {CapacitorUnit} from '../../models/capacitor-unit.model';
import {environment} from '../../../environments/environment';
import {ReCaptcha2Component} from '@niteshp/ngx-captcha';
import {Photo} from '../../models/file/photo.model';
import {scrollToElement} from '../../utilities/gui-utils';
import {ALL_UNITS} from '../../models/measurement-unit.model';
import {RefreshManufacturersService} from '../../services/refresh-manufacturers/refresh-manufacturers.service';
import {Measurement} from '../../models/measurement.model';

interface CapacitorForm {
  companyName: string;
  type: {
    typeNameSelect: string;
    typeContent: {
      typeNameInput: string;
      construction: string;
      constructionInput: string;
      startYear: number;
      endYear: number;
      description: string;
    }
  };
  unit: {
    capacitance: number
    voltage: number;
    notes: string;
    length: {lengthInput: string; lengthUnit: string};
    diameter: {diameterInput: string; diameterUnit: string};
    mountingHoleDiameter: {mountingHoleDiameterInput: string; mountingHoleDiameterUnit: string};
    thickness: {thicknessInput: string; thicknessUnit: string};
    photos: Array<Photo>;
  };
}

@Component({
  selector: 'app-capacitor-form',
  templateUrl: './capacitor-form.component.html',
  styleUrls: ['./capacitor-form.component.css', '../../styles/animations.css', '../../styles/expansion-panel.css']
})
export class CapacitorFormComponent implements OnInit, AfterViewInit {

  static readonly newConstructionOption = '+ Add Construction';

  editing = false;
  @Input('companyName') editCompanyName: string;
  @Input('capacitorType') editCapacitorType: CapacitorType;
  @Input('capacitorUnit') editCapacitorUnit: CapacitorUnit;

  @Input() only: 'type' | 'unit' | 'photos';

  capacitorFormGroup: FormGroup;
  submitting = false;
  errorsBackend: Array<SpringErrorResponse> = [];

  readonly noneSelected = 'Choose...';

  readonly allMeasurementUnits = ALL_UNITS;
  readonly defaultMeasurementUnit = ALL_UNITS[0];
  readonly invalidMeasurementMsg = 'Invalid format, e.g. \u200B 34, \u200B 7.5, \u200B 1/2 \u200B or \u200B 1-1/16';
  readonly tooLongMeasurementMsg = 'Measurement is too long';
  readonly maxMeasurementLen = 12;

  // Manufacturer Section
  readonly newManufacturerOption = '+ Add Manufacturer';
  companyNames$: Array<string> = [];
  isNavigatingToCreateManufacturer = false;
  loadingManufacturerList = false;

  // Type Section
  readonly newCapacitorTypeOption = '+ Add New Type';
  selectedCapacitorType: CapacitorType;
  capacitorTypes$: Array<CapacitorType> = [];
  readonly newConstructionOption = CapacitorFormComponent.newConstructionOption;
  constructionNames$: Array<string> = [];
  yearsAreExpanded = false;
  dimensionsAreExpanded = false;
  currentImageUploads = new Set<string>();
  loadingTypeList = false;

  // Unit Section
  @ViewChildren('unitElem') unitElemObservable: QueryList<any>;
  unitElem: ElementRef;

  // Captcha and Submit
  @ViewChild('captchaElem') captchaElem: ReCaptcha2Component;
  @ViewChild('submitDiv') submitDiv: ElementRef;
  readonly reCaptchaSiteKey = environment.reCaptchaSiteKey;


  constructor(private activatedRoute: ActivatedRoute,
              public restService: RestService,
              private router: Router,
              private formBuilder: FormBuilder,
              private refreshManufacturer: RefreshManufacturersService,
              public location: Location) { }

  ngOnInit(): void {
    this.getManufacturerList();
    this.getConstructionList();

    const integerPattern: RegExp = /^\d+$/;
    const decimalOrFractionPattern: RegExp = /^\s*((([0-9]*[.])?[0-9]*)|((?:\d+((\s*-\s*)|\s+))?\d+\s*\/\s*\d+))\s*$/;
    // True when !== this.noneSelected
    const noneSelectedPattern: RegExp = new RegExp('^(?!.*^' + this.noneSelected + '$)');
    this.capacitorFormGroup = this.formBuilder.group({
      companyName: ['', Validators.required],
      type: this.formBuilder.group({
        typeNameSelect: ['', Validators.required],
        typeContent: this.formBuilder.group({
          typeNameInput: [{value: '', disabled: true}, Validators.required],
          construction: [{value: this.noneSelected, disabled: true}, Validators.pattern(noneSelectedPattern)],
          constructionInput: [{value: '', disabled: true}, []],
          startYear: [{value: '', disabled: true}, [
            Validators.pattern(integerPattern), Validators.min(1000), Validators.max(new Date().getFullYear())]
          ],
          endYear: [{value: '', disabled: true}, [
            Validators.pattern(integerPattern), Validators.min(1000), Validators.max(new Date().getFullYear())]
          ],
          description: [{value: '', disabled: true}, []],
        }, {validator: [checkIfEndYearBeforeStartYear, checkNewConstruction]}),
      }),
      unit: this.formBuilder.group({
        capacitance: ['', this.only === 'type' ? [] : Validators.required],
        voltage: ['', [Validators.pattern(integerPattern)]],
        notes: ['', []],
        length: this.formBuilder.group({
          lengthInput: ['', [Validators.pattern(decimalOrFractionPattern), Validators.maxLength(this.maxMeasurementLen)]],
          lengthUnit: [this.defaultMeasurementUnit, []],
        }),
        diameter: this.formBuilder.group({
          diameterInput: ['', [Validators.pattern(decimalOrFractionPattern), Validators.maxLength(this.maxMeasurementLen)]],
          diameterUnit: [this.defaultMeasurementUnit, []],
        }),
        mountingHoleDiameter: this.formBuilder.group({
          mountingHoleDiameterInput: ['', [Validators.pattern(decimalOrFractionPattern), Validators.maxLength(this.maxMeasurementLen)]],
          mountingHoleDiameterUnit: [this.defaultMeasurementUnit, []],
        }),
        thickness: this.formBuilder.group({
          thicknessInput: ['', [Validators.pattern(decimalOrFractionPattern), Validators.maxLength(this.maxMeasurementLen)]],
          thicknessUnit: [this.defaultMeasurementUnit, []],
        }),
        photos: [new Array<Photo>(), []],
      }),
      captcha: ['', Validators.required],
    });

    // Setup for editing
    if (this.editCompanyName && this.editCapacitorType) {
      this.editing = true;
      this.populateFormFieldsEditing(this.editCompanyName, this.editCapacitorType, this.editCapacitorUnit);
      this.expandMenus();
      this.formFields.type.controls.typeContent.enable();
    }
  }

  ngAfterViewInit(): void {
    if (this.only === 'photos') {
      scrollToElement(this.submitDiv?.nativeElement, 'auto');
    }
  }

  /**
   * Populates the fields of the FormGroup when editing.  Uses the values from @Input
   */
  private populateFormFieldsEditing(companyName: string, capacitorType: CapacitorType, capacitorUnit: CapacitorUnit): void {
    this.capacitorFormGroup.patchValue({
      companyName,
      type: {
        typeNameSelect: capacitorType.typeName,
        typeContent: {
          typeNameInput: capacitorType.typeName,
          construction: capacitorType.constructionName,
          constructionInput: '',
          startYear: capacitorType.startYear,
          endYear: capacitorType.endYear,
          description: capacitorType.description
        }
      },
    });
    if (this.only !== 'type' && capacitorUnit) {
      this.capacitorFormGroup.patchValue({
        unit: {
          capacitance: capacitorUnit.capacitance,
          voltage: capacitorUnit.voltage,
          notes: capacitorUnit.notes,

          length: capacitorUnit.length ?
            {lengthInput: capacitorUnit.length.measurementStr, lengthUnit: capacitorUnit.length.unitStr} :
            {lengthUnit: this.defaultMeasurementUnit},

          diameter: capacitorUnit.diameter ?
            {diameterInput: capacitorUnit.diameter.measurementStr, diameterUnit: capacitorUnit.diameter.unitStr} :
            {diameterUnit: this.defaultMeasurementUnit},

          thickness: capacitorUnit.thickness ?
            {thicknessInput: capacitorUnit.thickness?.measurementStr, thicknessUnit: capacitorUnit.thickness?.unitStr} :
            {thicknessUnit: this.defaultMeasurementUnit},

          mountingHoleDiameter: capacitorUnit.mountingHoleDiameter ?
            {mountingHoleDiameterInput: capacitorUnit.mountingHoleDiameter?.measurementStr,
                                 mountingHoleDiameterUnit: capacitorUnit.mountingHoleDiameter?.unitStr} :
            {mountingHoleDiameterUnit: this.defaultMeasurementUnit},

          photos: capacitorUnit.getOrderedPhotos(),
        },
      });
    }
  }

  /**
   * Handle enter keypress inside a form group.  Only submits a form if enter is pressed within an input element.
   * @param event key event
   */
  handleEnterKeyPress(event): boolean {
    const tagName = event.target.tagName.toLowerCase();
    if (tagName === 'input') {
      this.onSubmit(this.capacitorFormGroup);
      return false;
    }
  }

  /** Expand all menus that have data in them, collapse all that don't */
  private expandMenus(): void {
    this.yearsAreExpanded = Boolean(this.typeFields.startYear.value || this.typeFields.endYear.value);
    this.dimensionsAreExpanded = Boolean(
      this.unitFields.length.controls.lengthInput.value ||
      this.unitFields.diameter.controls.diameterInput.value ||
      this.unitFields.mountingHoleDiameter.controls.mountingHoleDiameterInput.value ||
      this.unitFields.thickness.controls.thicknessInput.value
    );
  }

  /** Inserts a manufacturer name into the dropdown menu if it exists in the url */
  private populateManufacturerCreating(allCompanyNames: Array<string>): void {
    if (this.editing) {
      return;
    }

    const lowercaseCompanyName = this.activatedRoute.snapshot.paramMap.get('companyName');

    let companyName;
    allCompanyNames.forEach(name => {
      if (name.toLocaleLowerCase() === lowercaseCompanyName) {
        companyName = name;
      }
    });

    if (companyName) {
      this.capacitorFormGroup.controls.companyName.setValue(companyName);
      this.manufacturerMenuChanged(companyName);
    }

  }

  /** Inserts a type into the dropdown menu if it exists in the url */
  private populateTypeNameCreating(allTypes: Array<CapacitorType>): void {
    if (this.editing) {
      return;
    }

    const lowercaseTypeName = this.activatedRoute.snapshot.paramMap.get('typeName');

    let typeName;
    allTypes.forEach(type => {
      if (type.typeName.toLocaleLowerCase() === lowercaseTypeName) {
        typeName = type.typeName;
      }
    });

    if (typeName) {
      this.formFields.type.controls.typeNameSelect.setValue(typeName);
      this.typeMenuChanged(typeName);

      this.unitElemObservable.changes.subscribe(
        r => setTimeout(() => scrollToElement(r.first?.nativeElement, 'smooth'), 500)
      );
    }
  }

  /** Update this.companyNames$ */
  getManufacturerList(): Subscription {
    this.loadingManufacturerList = true;
    return this.restService.getAllCompanyNames().subscribe({
      next: companyNames => {
        companyNames.sort(caseInsensitiveCompare);
        this.companyNames$ = companyNames;
        this.populateManufacturerCreating(this.companyNames$);
        this.loadingManufacturerList = false;
      },

      error: () => console.error('Couldn\'t get company names')
    });
  }

  /** Update this.capacitorTypes$ */
  getTypeList(companyName: string): Subscription {
    this.loadingTypeList = true;
    return this.restService.getAllTypes(companyName).subscribe({
      next: types => {
        types.sort((a: CapacitorType, b: CapacitorType) => caseInsensitiveCompare(a.typeName, b.typeName));
        this.capacitorTypes$ = types;
        this.typeMenuChanged(this.noneSelected);
        this.populateTypeNameCreating(this.capacitorTypes$);
        this.loadingTypeList = false;
      },

      error: () => console.error('Couldn\'t get capacitor types')
    });
  }

  /** Update this.capacitorTypes$ */
  getConstructionList(): Subscription {
    return this.restService.getAllConstructions().subscribe({
      next: constructionNames => {
        constructionNames.sort(caseInsensitiveCompare);
        this.constructionNames$ = constructionNames;
      },

      error: () => console.error('Couldn\'t get construction names')
    });
  }

  /** Handle the event when a companyName is selected */
  manufacturerMenuChanged(value): void {
    if (value === this.newManufacturerOption) {

      this.gotoAddNewManufacturer();

    }

    if (this.manufacturerIsSelected) {
      this.formFields.type.controls.typeNameSelect.setValue('');  // Reset type menu
      this.getTypeList(value);
    }

    this.capacitorFormGroup.patchValue({
      companyName: value
    });

  }

  /** Handle the event when a typeName is selected */
  typeMenuChanged(selectedTypeName): void {

    // Inefficient O(n)
    this.selectedCapacitorType = this.capacitorTypes$.filter(ct => ct.typeName === selectedTypeName).pop();

    selectedTypeName === this.newCapacitorTypeOption ?
      this.formFields.type.controls.typeContent.enable() :
      this.formFields.type.controls.typeContent.disable();

    this.capacitorFormGroup.patchValue({
      type: {
        typeContent: {
          typeNameInput: this.selectedCapacitorType && this.selectedCapacitorType.typeName,
          construction: this.selectedCapacitorType ? this.selectedCapacitorType.constructionName : this.noneSelected,
          startYear: this.selectedCapacitorType && this.selectedCapacitorType.startYear,
          endYear: this.selectedCapacitorType && this.selectedCapacitorType.endYear,
          description: this.selectedCapacitorType && this.selectedCapacitorType.description,
        }
      }
    });
    this.yearsAreExpanded = Boolean(this.typeFields.startYear.value || this.typeFields.endYear.value);
  }

  /** Called when the button "Add a new manufacturer" is pressed */
  gotoAddNewManufacturer(): void {
    this.isNavigatingToCreateManufacturer = true;
    this.formFields.companyName.disable();

    setTimeout(() => {
      this.router.navigate(['/manufacturer', 'create']).catch(
        () => {
          this.isNavigatingToCreateManufacturer = false;
          this.formFields.companyName.enable();
        }
      );
    }, 700);
  }

  /** Called when the button "Add a new type" is pressed */
  gotoAddNewType(): void {
    this.capacitorFormGroup.patchValue({
      type: {
        typeNameSelect: this.newCapacitorTypeOption
      }
    });
    this.typeMenuChanged(this.newCapacitorTypeOption);
  }


  /**
   * Function called when submitting the form.  Calls submitCreateEditRecursive.
   * @param capacitorFormGroup form data
   */
  onSubmit(capacitorFormGroup: FormGroup): void {
    if (capacitorFormGroup.invalid || this.currentImageUploads.size > 0) {
      return;
    }

    this.submitting = true;
    this.errorsBackend = [];

    this.submitCreateEditRecursive(capacitorFormGroup.value);
  }


  /**
   * Used when creating and editing.  Sends one or more http requests to create/edit CapacitorType, Construction, and CapacitorUnit
   * depending on what form controls have been entered.  If multiple need to be created/edited, then this method is called recursively
   * after each successful http request is completed.
   * @param capacitorForm form data
   */
  submitCreateEditRecursive(capacitorForm: CapacitorForm): void {
    let httpRequestObservable;

    // Create new type
    if (!this.formFields.type.controls.typeContent.pristine) {
      const capacitorTypeFields = capacitorForm.type.typeContent;

      // Create new Construction
      if (capacitorTypeFields.construction === this.newConstructionOption) {
        return this.restService.createConstruction(capacitorTypeFields.constructionInput).subscribe({
          next: capitalizedConstructionName => {
            this.constructionNames$.push(capitalizedConstructionName);
            this.constructionNames$.sort(caseInsensitiveCompare);
            capacitorForm.type.typeContent.construction = capitalizedConstructionName;
            this.submitCreateEditRecursive(capacitorForm);
          },
          error: error => this.handleBackendError(error.error),
        });
      }

      capacitorTypeFields.typeNameInput = capacitorTypeFields.typeNameInput.trim();  // Mutation is needed for capacitor unit

      const capacitorType: CapacitorType = new CapacitorType();
      capacitorType.typeName = capacitorTypeFields.typeNameInput;
      capacitorType.startYear = capacitorTypeFields.startYear;
      capacitorType.endYear = capacitorTypeFields.endYear;
      capacitorType.description = capacitorTypeFields.description;
      capacitorType.companyName = capacitorForm.companyName;
      capacitorType.constructionName = capacitorTypeFields.construction;  // This is trimmed by the backend, so don't call trim()

      httpRequestObservable = this.editing ?
        this.restService.editCapacitorType(this.editCompanyName, this.editCapacitorType.typeName, capacitorType) :
        this.restService.createCapacitorType(capacitorType);

      return httpRequestObservable.subscribe({
        next: () => {
          this.formFields.type.controls.typeContent.markAsPristine();
          if (!this.editing) {this.refreshManufacturer.addedNewCapacitorType(capacitorType?.companyName); }
          this.submitCreateEditRecursive(capacitorForm);
        },
        error: error => this.handleBackendError(error.error),
      });

    }

    // Create Unit
    if (!this.formFields.unit.pristine) {

      const capacitorUnit: CapacitorUnit = new CapacitorUnit();
      capacitorUnit.capacitance = capacitorForm.unit.capacitance;
      capacitorUnit.voltage = capacitorForm.unit.voltage;
      capacitorUnit.notes = capacitorForm.unit.notes;

      capacitorUnit.length = Measurement.fromNullValues(capacitorForm.unit.length.lengthInput,
                                                        capacitorForm.unit.length.lengthUnit);
      capacitorUnit.diameter = Measurement.fromNullValues(capacitorForm.unit.diameter.diameterInput,
                                                          capacitorForm.unit.diameter.diameterUnit);
      capacitorUnit.mountingHoleDiameter = Measurement.fromNullValues(capacitorForm.unit.mountingHoleDiameter.mountingHoleDiameterInput,
                                                                      capacitorForm.unit.mountingHoleDiameter.mountingHoleDiameterUnit);
      capacitorUnit.thickness = Measurement.fromNullValues(capacitorForm.unit.thickness.thicknessInput,
                                                           capacitorForm.unit.thickness.thicknessUnit);

      capacitorUnit.setOrderedPhotos(capacitorForm.unit.photos);
      // Remove circular references
      capacitorUnit.photos.forEach(p => p.thumbnails.forEach(t => t.photo = null));
      capacitorUnit.typeName = capacitorForm.type.typeContent ?
        capacitorForm.type.typeContent.typeNameInput : capacitorForm.type.typeNameSelect;
      capacitorUnit.companyName = capacitorForm.companyName;

      httpRequestObservable = this.editing ?
        this.restService.editCapacitorUnit(this.editCompanyName, capacitorUnit.typeName, this.editCapacitorUnit.value,
          capacitorUnit) :
        this.restService.createCapacitorUnit(capacitorUnit);

      return httpRequestObservable.subscribe({
        next: (returnedCapacitorUnit: CapacitorUnit) => {

          if (!this.editing) {this.refreshManufacturer.addedNewCapacitorUnit(capacitorUnit?.companyName); }

          this.router.navigate([
            '/capacitor',
            'view',
            returnedCapacitorUnit.companyName,
            returnedCapacitorUnit.typeName,
            returnedCapacitorUnit.value
          ]);
          return;
        },
        error: error => this.handleBackendError(error.error),
      });

    }
    // Only executed if a unit isn't created/edited

    // If Capacitor Unit not given and type name has not changed go back a page on submit
    if (!this.editCapacitorUnit && this.editCompanyName && this.editCapacitorType &&
      this.editCapacitorType.typeName === capacitorForm.type.typeContent.typeNameInput) {
      this.location.back();
      return;
    }

    const route = [
      '/capacitor',
      'view',
      (this.editCompanyName || capacitorForm.companyName),
      ((capacitorForm.type.typeContent ? capacitorForm.type.typeContent.typeNameInput : capacitorForm.type.typeNameSelect)
        || this.editCapacitorType.typeName),
    ];
    if (this.editCapacitorUnit) { route.push(this.editCapacitorUnit.value); }
    this.router.navigate(route);

  }



  handleBackendError(error: SpringErrorResponse): void {
    this.submitting = false;
    this.errorsBackend.push(error);
  }

  get formFields(): any {
    return this.capacitorFormGroup.controls;
  }

  get typeFields(): any {
    return this.formFields.type.controls.typeContent.controls;
  }

  get unitFields(): any {
    return this.formFields.unit.controls;
  }

  get manufacturerIsSelected(): boolean {
    return !this.formFields.companyName.invalid;
  }

  get capacitorTypeIsSelected(): boolean {
    return !this.formFields.type.controls.typeNameSelect.invalid;
  }

  get endYearBeforeStartYearError(): boolean {
    return this.formFields.type.controls.typeContent.errors && this.formFields.type.controls.typeContent.errors.endYearBeforeStartYear;
  }

  get noNewConstructionEnteredError(): boolean {
    return this.formFields.type.controls.typeContent.errors && this.formFields.type.controls.typeContent.errors.noNewConstructionEntered;
  }

}

/**
 * If a + Add a Construction has been selected, nothing has been entered, return an error.
 * @param c form control for typeContent
 * @return error object { noNewConstructionEntered: true } or null
 */
function checkNewConstruction(c: AbstractControl): any {

  if (c.value.construction === CapacitorFormComponent.newConstructionOption && !c.value.constructionInput) {
    return { noNewConstructionEntered: true };
  } else {
    return null;
  }
}

function checkIfEndYearBeforeStartYear(c: AbstractControl): any {

  const startYear: number = parseInt(c.value.startYear, 10);
  const endYear: number = parseInt(c.value.endYear, 10);

  if (!startYear || !endYear) { return null; }

  return (startYear <= endYear) ? null : { endYearBeforeStartYear: true };
  // carry out the actual date checks here for is-endDate-after-startDate
  // if valid, return null,
  // if invalid, return an error object (any arbitrary name), like, return { invalidEndDate: true }
  // make sure it always returns a 'null' for valid or non-relevant cases, and a 'non-null' object for when an error should be raised on
  // the formGroup
}

package com.capacitorarchive.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MeasurementTest {

    @Test
    void oneMM() {
        Measurement measurement = new Measurement("1", Measurement.Unit.mm);
        assertEquals(1000000000L, measurement.getMeasurementPm());
    }

    @Test
    void oneCM() {
        Measurement measurement = new Measurement("1", Measurement.Unit.cm);
        assertEquals(10000000000L, measurement.getMeasurementPm());
    }

    @Test
    void oneIN() {
        Measurement measurement = new Measurement("1", Measurement.Unit.in);
        assertEquals(25400000000L, measurement.getMeasurementPm());
    }

    @Test
    void intMM() {
        Measurement measurement = new Measurement("12345", Measurement.Unit.mm);
        assertEquals(12345000000000L, measurement.getMeasurementPm());
    }

    @Test
    void floatMM() {
        Measurement measurement = new Measurement("3.145", Measurement.Unit.mm);
        assertEquals(3145000000L, measurement.getMeasurementPm());
    }

    @Test
    void floatIN() {
        Measurement measurement = new Measurement("3.145", Measurement.Unit.in);
        assertEquals(79883000000L, measurement.getMeasurementPm());
    }

    @Test
    void floatNoLeadingDigitMM() {
        Measurement measurement = new Measurement(".117", Measurement.Unit.mm);
        assertEquals(117000000L, measurement.getMeasurementPm());
    }

    @Test
    void fractionMM() {
        Measurement measurement = new Measurement("3/4", Measurement.Unit.mm);
        assertEquals(750000000L, measurement.getMeasurementPm());
    }

    @Test
    void repeatingFractionCM() {
        Measurement measurement = new Measurement("1/3", Measurement.Unit.cm);
        assertEquals(3333333333L, measurement.getMeasurementPm());
    }

}

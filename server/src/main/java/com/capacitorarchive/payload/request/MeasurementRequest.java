package com.capacitorarchive.payload.request;

import com.capacitorarchive.model.Measurement;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@EqualsAndHashCode
public class MeasurementRequest {


    @NotNull(message = "Measurement is missing an associated unit")
    @JsonProperty("unit")
    @Enumerated(EnumType.ORDINAL)
    private Measurement.Unit unit;

    @NotNull(message = "Measurement is missing a numerical value")
    @JsonProperty("measurementStr")
    private String measurementStr;

    public MeasurementRequest() {}

}

package com.capacitorarchive.payload.response;

import com.capacitorarchive.model.Measurement;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
@EqualsAndHashCode
public class MeasurementResponse {

    @JsonProperty("unit")
    @Enumerated(EnumType.ORDINAL)
    private Measurement.Unit unit;

    @JsonProperty("measurementStr")
    private String measurementStr;

    @JsonProperty("measurementPm")
    private Long measurementPm;

    public MeasurementResponse() {}

    public MeasurementResponse(Measurement measurement) {
        Measurement m = measurement;
        setUnit(m.getUnit());
        setMeasurementStr(m.getMeasurementStr());
        setMeasurementPm(m.getMeasurementPm());
    }

    /**
     * Constructs a new MeasurementResponse only if Measurement is non-null.
     * Else returns null.
     * @param measurement a Measurement that may be null/undefined
     */
    public static MeasurementResponse fromNullValues(Measurement measurement) {
        return measurement != null ? new MeasurementResponse(measurement) : null;
    }
}

package com.capacitorarchive.payload.response;

import com.capacitorarchive.model.CapacitorUnit;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@EqualsAndHashCode
public class CapacitorUnitResponse {

    @JsonProperty("capacitance")
    private Long capacitance;

    @JsonProperty("voltage")
    private Integer voltage;

    @JsonProperty("value")
    private String value;

    @JsonProperty("notes")
    private String notes;

    @JsonProperty("length")
    private MeasurementResponse length;

    @JsonProperty("diameter")
    private MeasurementResponse diameter;

    @JsonProperty("thickness")
    private MeasurementResponse thickness;

    @JsonProperty("mountingHoleDiameter")
    private MeasurementResponse mountingHoleDiameter;

    @JsonProperty("typeName")
    private String typeName;

    @JsonProperty("companyName")
    private String companyName;

    @JsonProperty("photos")
    private List<PhotoResponse> photos = new ArrayList<>();


    public CapacitorUnitResponse(CapacitorUnit capacitorUnit) {
        CapacitorUnit cu = capacitorUnit;

        setCapacitance(cu.getCapacitance());
        setVoltage(cu.getVoltage());
        setValue(cu.getValue());
        setNotes(cu.getNotes());

        setLength(MeasurementResponse.fromNullValues(cu.getLength()));
        setDiameter(MeasurementResponse.fromNullValues(cu.getDiameter()));
        setMountingHoleDiameter(MeasurementResponse.fromNullValues(cu.getMountingHoleDiameter()));
        setThickness(MeasurementResponse.fromNullValues(cu.getThickness()));

        setTypeName(cu.getCapacitorType().getTypeName());
        setCompanyName(cu.getCapacitorType().getManufacturer().getCompanyName());

        // Convert Photo List to PhotoResponse list
        setPhotos(cu.getPhotos().stream().map(PhotoResponse::new).collect(Collectors.toList()));
    }

    public CapacitorUnitResponse() {}
}

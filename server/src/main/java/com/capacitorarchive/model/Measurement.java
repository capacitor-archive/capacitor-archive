package com.capacitorarchive.model;

import com.capacitorarchive.payload.request.MeasurementRequest;
import com.capacitorarchive.utility.NumberParser;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@EqualsAndHashCode
@Entity
public class Measurement {

    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    @Transient
    private final int MEASUREMENT_LEN = 20;

    public enum Unit {mm, cm, in}

    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    @Transient
    private final long PM_PER_INCH = 25400000000L;

    @Id
    @Setter(AccessLevel.NONE)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "unit")
    private Unit unit;

    @Setter(AccessLevel.NONE)
    @Column(name = "measurement_str", length = MEASUREMENT_LEN)
    private String measurementStr;

    /** The length of the measurement in pico-meters */
    @Setter(AccessLevel.NONE)
    @Column(name = "measurement_pm")
    private Long measurementPm;

    /**
     * Converts the measurement string into pico meters and saves the result in measurementPm.
     */
    @PrePersist
    @PreUpdate
    private void generateMeasurementPm() {

        BigDecimal number;
        try {
            number = NumberParser.parse(this.measurementStr);
        } catch (NumberFormatException | NullPointerException e) {
            return;
        }

        switch (this.unit) {
            case mm:
                this.measurementPm = number.scaleByPowerOfTen(9).longValue();
                break;

            case cm:
                this.measurementPm = number.scaleByPowerOfTen(10).longValue();
                break;

            case in:
                this.measurementPm = number.multiply(new BigDecimal(PM_PER_INCH)).longValue();
                break;
        }
    }

    public Measurement() { }

    public Measurement(String measurementStr, Unit unit) {
        setUnit(unit);
        setMeasurementStr(measurementStr);
    }

    public Measurement(MeasurementRequest measurementRequest) {
        MeasurementRequest r = measurementRequest;
        setUnit(r.getUnit());
        setMeasurementStr(r.getMeasurementStr());
    }

    /**
     * Constructs a new Measurement only if measurementStr and unit are non-null and measurementStr is not empty.
     * Else returns null.
     * @param measurementStr a string that may be null/undefined or empty
     * @param unit a unit that may be null/undefined
     */
    public static Measurement fromNullValues(String measurementStr, Unit unit) {
        return (measurementStr != null && unit != null && measurementStr.length() > 0) ?
                new Measurement(measurementStr, unit) : null;
    }

    /**
     * Constructs a new Measurement only if MeasurementRequest is non-null.
     * Else returns null.
     * @param measurementRequest a MeasurementRequest that may be null/undefined
     */
    public static Measurement fromNullValues(MeasurementRequest measurementRequest) {
        return measurementRequest != null ? new Measurement(measurementRequest) : null;
    }


    public void setMeasurementStr(String measurementStr) {
        this.measurementStr = measurementStr
                .replaceAll("\\s", "")
                .replaceAll("\\.$", "");
        generateMeasurementPm();
    }
}
